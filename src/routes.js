import Home from './components/home/index'
import Login from './components/auth/login'
import CreateConta from './components/contas/newcreate'
import CreateSaque from './components/saque/create'
import DepositoSaque from './components/deposito/create'

const routes = [{
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/home',
        name: 'home',
        component: Home
    },
    {
        path: '/deposito/store',
        name: 'deposito-create',
        component: DepositoSaque
    },
    {
        path: '/auth/login',
        name: 'login',
        component: Login
    },
    {
        path: '/conta/store',
        name: 'conta-create',
        component: CreateConta,
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/saque/store',
        name: 'saque-create',
        component: CreateSaque,
        meta: {
            requiresAuth: true,
        }
    }
]

export default routes