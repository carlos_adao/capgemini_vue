import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://api.capgemini.com.br/api'

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        msg: null,
    },
    getters: {
        //retorna o jwt se o usuário estiver logado 
        loggedIn(state) {
            return state.token !== null
        },

        //retorna o status das requisicao
        statusRequisicao(state, getters) {
            return getters.remaining != 0
        },
    },
    mutations: {
        //atualiza o status da mensagem para saber de não deu erro
        createDeposito(state, res) {
            state.msg = res;
        },

        //recuperar o token 
        retrieveToken(state, token) {
            state.token = token
        },

        //deletar o token 
        destroyToken(state) {
            state.token = null
        },
    },
    actions: {
        //retorna o nome do usuário logado
        retrieveName(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve, reject) => {
                axios.get('/user')
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        //Deleta o token para encerrar a sessão
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post('/logout')
                        .then(response => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(error => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            reject(error)
                        })
                })
            }
        },

        //Recupera o JWT para salvar no local storage 
        loginConta(context, credentials) {
            return new Promise((resolve, reject) => {
                axios.post('auth/login', {
                        email: "admim@apicapgemini.com.br",
                        password: credentials.password,
                    })
                    .then(response => {
                        const token = response.data.access_token
                        localStorage.setItem('access_token', token)
                        context.commit('loginConta', token)
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        reject(error)
                    })
            })
        },

        //Método usado para solicita para API cadastro conta
        //Create by Adao 26.08.2020
        createConta(context, conta) {
            axios.post('/conta/store', {
                    cpf: conta.cpf,
                    nome: conta.nome,
                    tipo: conta.tipo,
                })
                .then(response => {
                    context.commit('createConta', response.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },

        //Método usado para solicita para API cadastro saque 
        //Create by Adao 26.08.2020
        createSaque(context, saque) {
            return new Promise((resolve, reject) => {
                axios.post('/saque/store', {
                        user_id: 1,
                        valor: saque.valor,
                        password: saque.senha,
                        t_conta: saque.tipo,
                    })
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        reject(error.data)
                    })
            })
        },

        //Método usado para solicita para API cadastro deposito
        //Create by Adao 26.08.2020
        createDeposito(context, dep) {
            return new Promise((resolve, reject) => {
                axios.post('/deposito/store', {
                        num_conta: dep.num_conta,
                        tipo: dep.tipo,
                        valor: dep.valor,
                        tel: dep.tel
                    })
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(error => {
                        reject(error.data)
                    })
            })
        },
    }
})